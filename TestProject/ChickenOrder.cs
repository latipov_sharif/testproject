﻿namespace TestProject {
    class ChickenOrder {

        private readonly int _quantity;

        public ChickenOrder(int quantity) {
            _quantity = quantity;
        }

        public int GetQauntity() {
            return _quantity;
        }

        public void CutUp() {
        }

        public void Cook() {
        }

    }
}
