﻿using System;

namespace TestProject {
    class EggOrder {

        private readonly int _quantity;
        private readonly int? _quality;
        private static short forget = 0;

        public EggOrder(int quantity) {
            _quantity = quantity;
            var rnd = new Random();
            _quality = rnd.Next(1, 101);
        }


        public int GetQauntity() {
            return _quantity;
        }


        public int? GetQuality() {
            forget++;
            return (forget % 2 == 0)?_quality : null;
            
        }

        public void Crack() {
            if(_quality < 25) {
                throw new Exception("Egg is rotten");
            }
        }

        public void DiscardShell() {
        }

        public void  Cook() {
            
        }

    }
}
