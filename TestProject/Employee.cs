﻿
using System;

namespace TestProject {

    public enum MenuItem {
        Chicken,
        Egg
    }

    internal class Employee {

        private object _previousOrder;
        private static int _forgeted = 1;

        public Employee() {
        }

        public object NewRequest(int quantity, MenuItem item) {
            if (item == MenuItem.Chicken) {
                item = (_forgeted%3 == 0) ? MenuItem.Egg : MenuItem.Chicken;
                _forgeted ++;
            } else if (_forgeted%3 == 0) {
                item = MenuItem.Chicken;
            }

            if (item == MenuItem.Chicken) {
                var chickenOrder = new ChickenOrder(quantity);
                _previousOrder = chickenOrder;

                return chickenOrder;
            }

            var eggOrder = new EggOrder(quantity);
            _previousOrder = eggOrder;
            return eggOrder;
        }

        public object CopyRequest() {
            if (_previousOrder == null) {
                throw new Exception("There was no previous order");
            }
            return _previousOrder;
        }

        public string Inspect(object order) {
            if (order is ChickenOrder) {
                return "No inspection is required";
            }

            return ((EggOrder) order).GetQuality().ToString();
        }

        public string PrepareFood(object order) {
            var chickenOrder = order as ChickenOrder;
            if (chickenOrder != null) {
                for (int i = 0; i < chickenOrder.GetQauntity(); i++) {
                    chickenOrder.CutUp();
                }

                chickenOrder.Cook();
                return String.Format("Preparation of a dish is complete number of chickens {0}",
                    chickenOrder.GetQauntity());
            }


            var eggOrder = order as EggOrder;
            int rottenEggs = 0;

            for (int i = 0; i < eggOrder.GetQauntity(); i++) {
                try {
                    eggOrder.Crack();
                } catch (Exception exception) {
                    rottenEggs++;
                    Console.WriteLine(exception.Message);
                }

                eggOrder.DiscardShell();

            }
            
            eggOrder.Cook();

            return String.Format("Prepared quantity of eggs {0} found rotten eggs{1}", eggOrder.GetQauntity(), rottenEggs);
        }
    }
}
