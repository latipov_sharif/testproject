﻿using System;
using System.Windows;

namespace TestProject {
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window {

        private Employee _employee = new Employee();
        object _order; 

        public MainWindow() {
            InitializeComponent();
        }

        private void button1_Click(object sender, RoutedEventArgs e) {
            MenuItem item = new MenuItem();

            if (rbChicken.IsChecked == true) {
                item = MenuItem.Chicken;
            } else if (rbEgg.IsChecked == true) {
                item = MenuItem.Egg;
            }

            int quantity;
            try {
                quantity = int.Parse(txtQuantity.Text);
            } catch (Exception) {
                txtQuantity.Text = "1";
                quantity = 1;
            }

            _order = _employee.NewRequest(quantity, item);
            lblResult.Content = _employee.Inspect(_order);

        }

        private void button2_Click(object sender, RoutedEventArgs e) {
            try {
                _order = _employee.CopyRequest();
                try {
                    lblResult.Content = _employee.Inspect(_order);
                } catch (Exception ex) {
                    lblResult.Content = ex.Message;
                }
            } catch (Exception exception) {
                lblResult.Content = exception.Message;
            }
        }

        private void button3_Click(object sender, RoutedEventArgs e) {
            
            if (_order == null) {
                lblResult.Content = "You can't prepare food more than one time";
                return;
            }

            lblResult.Content = _employee.PrepareFood(_order);
            _order = null;
        }
    }
}
